import React, { Component } from 'react'
import { classNames } from '@utils'
import * as styles from './ScrollPanel.less'


export default class View extends Component {

    render() {
        const { style = {}, header, children, tableHeight = 'auto' } = this.props;
        return (
            <div className={classNames(
                styles.scrollPannel,
            )} style={style}>
                {header ? (<div className={styles.header} >{header}</div >) : null}
                <div className={styles.scrollPannelContainer} style={{ height: tableHeight }}>
                    <div className={styles.scrollPannelContent} >
                        {children}
                    </div>
                </div>
            </div>
        )
    }
}