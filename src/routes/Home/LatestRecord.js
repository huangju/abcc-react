import React, { Component } from 'react'
import { classNames } from '@utils'
import styles from './index.less'
import ScrollPannel from './components/ScrollPanel'
import { Table } from "@components"

export default class LatestRecord extends Component {
    startInit = () => {
        this.getLatestRecord()
    }

    getLatestRecord = () => {
        const { dispatch, modelName } = this.props
        dispatch({
            type: `${modelName}/getLatestRecord`,
        }).then(res => {
            if (res) {
                this.getLatestRecordFromWs()
            }
        })
    }

    render() {
        const columns = [{
            title: '时间',
            dataIndex: 'time',
            width: '25%',
        }, {
            title: '方向',
            dataIndex: 'type',
            width: '20%',
        }, {
            title: '价格',
            dataIndex: 'price',
            width: '35%',
        }, {
            title: '数量(张)',
            dataIndex: 'amount',
        }]

        const tableProps = {
            columns,
        }

        console.log('tableProps', tableProps);
        return (
            <div className={
                classNames({
                    view: true,
                }, styles.latestRecord)
            }>
                <ScrollPannel header={
                    <div className={styles.record_header} >
                        <span >最新成交</span >
                    </div>
                }>
                    <Table {...tableProps} />
                </ScrollPannel>
            </div>
        )
    }
}