import React, { Component } from 'react'
import styles from './index.less'
import LatestRecord from './LatestRecord'

const Comp = {
    LatestRecord,
}

export default class View extends Component {

    renderView = (name) => {

        const RenderItem = Comp[name];
        return <RenderItem/>
    }

    render() {
        return (
            <div className={styles.home} >
                <div className={styles.views}>
                    {this.renderView('LatestRecord')}
                </div>
            </div>
        )
    }
}