import { joinModel } from '@utils'
import modelExtend from '@models/modelExtend'

export default joinModel(modelExtend, {
    namespace: 'user',
    state: {
        data: null,
        name: '',
        state: false,
    },
    effects: {},
    reducers: {},
})