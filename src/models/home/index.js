import { joinModel } from '@utils'
import modelExtend from '@models/modelExtend'

export default joinModel(modelExtend, {
    namespace: 'home',
    state: {
        myname: 'home',
        count: 0,
    },
    subscriptions: {},
    effects: {
        * getLatestRecord({ payload = {} }, { call, put }) {
            
        }
    },
    reducers: {},
})