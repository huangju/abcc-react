import { joinModel, getRes, resOk } from '@utils'
import modelExtend from '@models/modelExtend'
import { getIndexInfo } from '@services/user.js';

export default joinModel(modelExtend, {
    namespace: 'dashboard',
    state: {
        myname: 'home',
        count: 0,
    },
    subscriptions: {},
    effects: {
        // * getIndexInfo
        * getIndexInfo({ payload = {} }, { call, put, select }) { // 获取google验证信息
            console.log('准备请求');
            const res = getRes(yield call(getIndexInfo, payload));
            console.log('res', res);
            if (resOk(res)) {
                return res;
            }
            return false;
        },
    },
    reducers: {
        add(count) { return count + 1 },
    },
})