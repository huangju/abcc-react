import { joinModel} from '@utils'
import modelExtend from '@models/modelExtend'

export default joinModel(modelExtend, {
    namespace: 'asset',
    state: {
        myname: 'home',
        count: 0,
    },
    subscriptions: {},
    effects: {},
    reducers: {},
})