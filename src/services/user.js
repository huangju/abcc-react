import { request } from '@utils'

export async function getIndexInfo(payload) {
    return await request('/api/v1/gateway/Home/Index', {
        query: {
            ...payload,
            type: 'Web'
        }
    })
}