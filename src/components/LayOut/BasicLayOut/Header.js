import { Component } from "react";
import { classNames } from '@utils'
import { netWorkBest, netWorkGood, netWorkBad, notice, help, account, logo } from '@assets'
import { RouterGo, Mixin } from '@components'
import { PATH, } from '@constants'
import * as styles from './index.less'
import { NavLink } from "dva/router";


export default class View extends Component {
    render() {

        const { routesBasic, location: { pathname } = {} } = this.props;

        const isMatch = (path) => {
            return pathname === path
        }

        const cla = (item = {}) => {
            return classNames(
                styles.navli,
                isMatch(item.path) ? styles.active : null
            )
        }

        return (
            <div className={styles.header}>
                <div className={styles.left}>
                    <RouterGo.SwitchRoute value={PATH.dashboard} >
                        {logo}
                    </RouterGo.SwitchRoute >
                    <ul className={styles.nav}>
                        {
                            routesBasic.map((item = {}, index) => {
                                return (
                                    <li key={index} className={cla(item)}>
                                        <NavLink to={item.path}>
                                            {item.name}
                                        </NavLink>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}