import React, { Component } from 'react'
import { Switch } from 'dva/router'
import dynamic from 'dva/dynamic'
import { classNames } from '@utils'
import Authority from './Authority'
import Header from './Header'
import Content from './Content'
import Footer from './Footer'
import * as styles from './index.less'

export default class View extends Component {
    render() {
        const { app, routesBasic } = this.props;
        return (
            <div className={
                classNames(
                    styles.overbody,
                    styles.dark,
                )}>
                <Header {...this.props}></Header>
                <Content>
                    <Switch>
                        {routesBasic.map(({ path, authority, ...dynamics }) => {
                            return <Authority key={path} path={path} exact component={dynamic({
                                app,
                                ...dynamics
                            })} />
                        })}
                    </Switch>
                </Content>
                <Footer {...this.props}></Footer>
            </div>
        )
    }
}