import React, { Component } from 'react'
import { connect } from 'dva'
import { classNames } from '@utils'
import * as styles from './index.less'


@connect(({ theme }) => ({
  theme
}))
export default class View extends Component {
  render() {
    const { children, theme: { theme } } = this.props

    return (
      <div id='overContent' className={
        classNames(
          styles.content,
          theme
        )}>
        {children}
      </div>
    )
  }
}
