import React, { Component } from 'react'
import { Route } from 'dva/router'

export default class Router extends Component {
    render() {
        const { component: Component } = this.props
        return (
            <Route render={props => <Component {...props} />} />
        )
    }
}
