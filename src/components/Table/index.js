import React, { Component } from 'react'
import { _, classNames, getPercent, isEqual, clearIntervals } from '@utils'
import { Scroller } from '@components'
import * as styles from './index.less'

const createElement = (_className) => {
    return (Props) => {
        const { className, style, children } = Props
        return (
            <div className={
                classNames(
                    { [_className]: true },
                    className
                )
            } style={style}>{children}</div >
        )
    }
}

const [Table, Thead, Tbody, Tr, Th, Td] = [
    createElement('table'),
    createElement('thead'),
    createElement('tbody'),
    createElement('tr'),
    createElement('th'),
    createElement('td'),
]


export default class View extends Component {

    constructor(props) {
        super(props)
        this.state = {
            x: 0,
        }
    }

    render() {
        const {
            scroll = {},
            columns = [],
            dataSource = [],
        } = this.props;
        return (
            <div className={classNames(
                styles.tableContainer,
            )} >
                <Table className={styles.table}>
                    <Thead style={{ left: this.state.x }} >
                        <Tr>
                            {columns.map((item = {}, index) => (
                                <Th key={index}>{item.title}</Th >
                            ))}
                        </Tr>
                    </Thead>
                    <div className={styles._scrollerTableContainer} >
                        <div className={styles._scrollerTable} >
                            <Scroller>
                                <Tbody>
                                    {

                                    }
                                </Tbody>
                            </Scroller>
                        </div>
                    </div>
                </Table>
            </div>
        )
    }
}