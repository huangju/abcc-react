import React from 'react'
import pathToRegexp from 'path-to-regexp'
import { Router, Route, Switch } from 'dva/router'
import { BasicLayOut } from '@components'
import { default as routes } from '@common/routes'

const getDynamicRoutes = (routes) => {
  return routes.map(item => {
    const { path, model: models, route: component, ...rest } = item
    // 把routes的item所需要的models,component都import
    return {
      path,
      models: models && models.length ? () => models.map(model => import(`@models/${model}`)) : undefined,
      component: component ? () => import(`@routes/${component}`) : console.error('新增的路由必须有对应的组件'),
      ...rest
    }
  })
}

const classifyRoutes = (routes) => {
  // 区分path是否含有user
  const [routesBasic, routesUser] = [[], []]
  routes.map(item => {
    if (pathToRegexp(`/user/(.*)?`).exec(item.path)) {
      return routesUser.push(item)
    } else {
      return routesBasic.push(item)
    }
  })
  return [routesBasic, routesUser]
}

export default ({ history, app }) => {
  // model
  routes[0].model.map(item => app.model(require(`@models/${item}`).default))
  // [0,1,2,3,4,5] => [0]
  // 去掉routes第一个
  routes.splice(0, 1)
  // 基本框架、user框架
  const [routesBasic, routesUser] = classifyRoutes(getDynamicRoutes(routes))
  const getProps = (props) => ({
    ...props,
    app,
    routesBasic,
    routesUser
  })
  console.log('routesBasic',routesBasic);
  return (
    <Router history={history} >
      <Switch>
        <Route path="/" render={(props) => (<BasicLayOut {...getProps(props)} />)} />
      </Switch>
    </Router>
  )
}
