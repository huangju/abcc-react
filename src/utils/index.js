import { lodash_helper } from './helper';

export { default as joinModel } from 'dva-model-extend';
export { request } from './request';
export classNames from 'classnames';
export const _ = lodash_helper;


export const getRes = function (res) {
    if (res) {
        return {
            head: _.get(res, 'data.head') || _.get(res, 'head') || {},
            data: _.has(res, 'data.data.data') ? _.get(res, 'data.data.data') : (_.has(res, 'data.data') ? _.get(res, 'data.data') : (_.has(res, 'data') ? _.get(res, 'data') : res))
        }
    }
    return {
        data: null,
        head: null,
        code: (res && res.errcode) || '',
        msg: _.get(res, 'data.errormsg') || _.get(res, 'errStr')
    }
}

export const resOk = (res, method) => {
    if (_.isNil(res.data)) {
        return false
    }
    return true
}